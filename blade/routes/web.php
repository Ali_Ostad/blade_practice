<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


//Route::get('/' , "HomeController@index")->name('index');

//Route::get('/hello/{name?}/{width?}/{height?}' , "HomeController@hello")->name('hello');

Route::get('/' , function (){
    return view('layouts.main');
});


Route::get('/hello' , function (){
    return view('hello');
});


Route::get('/upload-file' , function (){
   return view('file');
});


Route::get('/table' , "HomeController@ShowTable");


Route::get('/downloadFile' , "HomeController@downloadFile")->name("downloadFile");


Route::post('/getData' , "HomeController@Hello")->name("getData");


Route::post('/getFile' , "HomeController@File")->name("getFile");


Route::get('/DB' , 'UserController@index');

<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class WeakPassword implements Rule
{
    public  $weakPassword = array(
        "123456",
        "000000",
        "111111",
        "aliostad"
    );
    private $name;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !in_array($value , $this->weakPassword);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'پسورد شما ضعیف است';
    }
}

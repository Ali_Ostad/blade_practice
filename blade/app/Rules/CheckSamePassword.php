<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckSamePassword implements Rule
{
    public $name;
    public $email;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($name , $email)
    {
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if (strpos($value , $this->name) !== false){
            return false;
        }elseif (strpos($value , $this->email) !== false){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'نام یا یوزرنیم شما نباید شامل پسورد باشد';
    }
}

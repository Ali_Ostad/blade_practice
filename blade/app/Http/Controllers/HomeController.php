<?php


namespace App\Http\Controllers;


use App\Http\Requests\StoreBlogPost;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HomeController
{

    /*    public function index()
        {
            return route('hello' , ["name" => "avatar.png" , "test" => "mytest"]);
        }
    */


    /**
     * @param Request $request
     */
    public function Hello(StoreBlogPost $request)
    {

        $users = DB::table('MyUsers')->insert(
            ['name'=>$request->input('name'),'username'=>$request->input('family'),'email'=>$request->input('email'),'password'=>$request->input('password'),'mobile'=>'0987654']
        );
        return $users;
    }

    /**
     *
     */
    public function File(Request $request)
    {
        $file = $request->file('file')->store('/file-upload');

        return $file;


    }

    public function ShowTable()
    {
        $data = [
            [
                "id" => "1",
                "title" => "title1",
                "name" => "ali"
            ],
            [
                "id" => "2",
                "title" => "title2",
                "name" => "hasan"
            ],
            [
                "id" => "3",
                "title" => "title3",
                "name" => "hosein"
            ]
        ];

        return view('table', compact('data'));
    }

    public function downloadFile()
    {

        $data = [
            ["id" => "1", "title" => "title1", "name" => "ali"],
            ["id" => "2", "title" => "title2", "name" => "hasan"],
            ["id" => "3", "title" => "title3", "name" => "hosein"]
        ];

        $view = view('table' , compact('data'));

        $result = $view->render();

        Storage::put('file1.html' , $result);

        return Storage::download('file1.html');

    }
}

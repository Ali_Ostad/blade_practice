@extends('layouts.main')

@section('title' , "Hello Laravel")

@section("stylesheet")
@parent
@endsection
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h2 class="text-center">Form GEt Data</h2>
    <form action="{{route('getData')}}" method="post">
        @csrf

        <div class="form-group">

            <lable for="name">name :</lable>
            <input type="text" class="@error('name') is-invalid @enderror form-control"  name="name" id="name" placeholder="Name">
        </div>
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">

            <lable for="name">family :</lable>
            <input type="text" class="@error('family') is-invalid @enderror form-control"  name="family" id="name" placeholder="Family">
        </div>
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror


        <div class="form-group">
            <lable for="name">Email :</lable>
            <input type="text" class="@error('email') is-invalid @enderror form-control" name="email" id="name" placeholder="Email">
        </div>
        @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <lable for="name">Password :</lable>
            <input type="text" class="@error('email') is-invalid @enderror form-control" name="password" id="name" placeholder="Password">
        </div>
        @error('password')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror




        <input class="btn btn-info" type="submit" value="send">

    </form>


@endsection


@section('test')
    @parent

    <p>Ali</p>

    <p>Ostad</p>
@endsection

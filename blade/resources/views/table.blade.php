@extends('layouts.main')

@section('title' , "Show Table")

@section('content')

<table class="table table-striped">

    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Name</th>
    </tr>
@foreach($data as $value)
        <tr>
            <th>{{$value['id']}}</th>
            <th>{{$value['title']}}</th>
            <th>{{$value['name']}}</th>
        </tr>
@endforeach
</table>


@endsection

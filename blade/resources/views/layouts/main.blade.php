<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    @section('stylesheet')
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    @show
</head>

<body>
<div class="container">
    <div class="row justify-content-md-center mt-5">
        <div class="col-md-5">

            @yield('content')

        </div>
    </div>

    <div class="row">
        @section('test')
            <p>--  hi : hi  --</p>

            <p>-- bye : bye --</p>
        @show
    </div>
</div>


</body>
</html>
